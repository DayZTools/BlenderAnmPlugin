from typing import Union, List

from dayzAnmPlugin.Data.BasicTypes import Vector3, Vector2, Quaternion


class Lod:
    def __init__(self):
        self.compressionType: str = ''
        self.uvVec: Vector2 = Vector2()
        self.bonesCnt: int = 0
        self.offset: int = 0
        self.length: int = 0
        self.meshes: List[Mesh] = []
        self.bunks: List[UnkBoneClass] = []


class BoneTree:
    def __init__(self):
        self.leftIdx: int = 0
        self.rightIdx: int = 0
        self.parentIdx: int = 0
        self.name: str = ''
        self.position: Union[Vector3, None] = None
        self.rotation: Union[Quaternion, None] = None


class Mesh:
    def __init__(self):
        self.boneIndexes: dict[int, int] = {}
        self.rootName: str = ''
        self.materialName: str = ''
        self.materialCount: int = 0
        self.facesCount: int = 0
        self.verticesCount: int = 0
        self.unkCnt: int = 0
        self.weightsCount: int = 0
        self.unkCnt2: int = 0
        self.bonesDataCnt: int = 0


class UnkBoneClass:
    def __init__(self):
        self.unk1: str = ''
        self.unk2: str = ''
        self.vunk1: Vector3 = Vector3()
        self.vunk2: Vector3 = Vector3()


class Vertex:
    def __init__(self):
        self.position: Vector3 = Vector3()
        self.normal: Vector3 = Vector3()
        self.uvLayers: List[Vector2] = []
        self.weightIndex: int = 0
