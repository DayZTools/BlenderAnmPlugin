from typing import List, Union

from dayzAnmPlugin.Data.BasicTypes import Vector3, Quaternion


class KeyFrame:
    def __init__(self, frame, data):
        self.frame: int = frame
        self.data: Union[Vector3, Quaternion] = data


class BoneHead:
    def __init__(self):
        self.name: str = ''
        self.translationBias: float = 0
        self.translationMultiplier: float = 0
        self.scaleBias: float = 0
        self.scaleMultiplier: float = 0
        self.rotationBias: float = 0
        self.rotationMultiplier: float = 0
        self.numFrames: int = 0
        self.translationFrameCount: int = 0
        self.scaleFrameCount: int = 0
        self.rotationFrameCount: int = 0
        self.flags: int = 0


class BoneAnimation:
    def __init__(self):
        self.frameCount: int = 0
        self.translations: List[KeyFrame] = []
        self.scales: List[KeyFrame] = []
        self.rotations: List[KeyFrame] = []


class AnimEvent:
    def __init__(self):
        self.frame: int = 0
        self.name: str = ''
        self.str2: str = ''
        self.unk1: int = 0
