class Vector2:
    def __init__(self):
        self.x: float = 0
        self.y: float = 0

    def toArray(self):
        return [self.x, self.y]

class Vector3:
    def __init__(self):
        self.x: float = 0
        self.y: float = 0
        self.z: float = 0

    def toArray(self):
        return [self.x, self.y, self.z]


class Quaternion:
    def __init__(self):
        self.x: float = 0
        self.y: float = 0
        self.z: float = 0
        self.w: float = 0

    def toArray(self):
        return [self.x, self.y, self.z, self.w]