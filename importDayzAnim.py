import time
import bpy
from bpy.props import StringProperty, CollectionProperty
from bpy_extras.io_utils import ImportHelper


class ImportDayzAnim(bpy.types.Operator, ImportHelper):
    bl_idname = "import_scene.dayzanm"
    bl_label = "Import DayZ .anm"
    bl_description = "Import one or more ANM files"
    bl_options = {'PRESET'}

    filename_ext = ".anm"
    filter_glob: StringProperty(default="*.anm", options={'HIDDEN'})

    files: CollectionProperty(type=bpy.types.PropertyGroup)

    def execute(self, context):
        from . import DayZAnim_import
        start_time = time.process_time()
        result = DayZAnim_import.load(self, context, **self.as_keywords(ignore=("filter_glob", "files")))
        if not result:
            self.report({'INFO'}, "Import finished in %.4f sec." % (time.process_time() - start_time))
            return {'FINISHED'}
        else:
            self.report({'ERROR'}, result)
            return {'CANCELLED'}

    @classmethod
    def poll(self, context):
        if context.active_object is not None:
            if context.active_object.type == 'ARMATURE':
                return True

            # Currently Disabled
            """
            elif context.active_object.parent is not None:
                return context.active_object.parent.type == 'ARMATURE'
            """

        return False
