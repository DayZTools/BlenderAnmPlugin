import io
import os
import struct
from abc import ABC
from typing import IO, BinaryIO, Union

from dayzAnmPlugin.Data.BasicTypes import Vector3, Vector2, Quaternion


class BinaryStream:
    def __init__(self):
        self.stream: Union[io.BytesIO, None] = None
        self.length: int = 0

    def readString(self, length: int) -> str:
        s = self.stream.read(length).decode('ascii')
        i = 0
        while i < length:
            if s[i] == '\0':
                break
            i += 1
        return s[0:i]

    def readNullString(self) -> str:
        return ''.join(iter(lambda: self.stream.read(1).decode('ascii'), '\0'))

    def readSInt(self) -> int:
        return int.from_bytes(self.stream.read(4), "little", signed=True)

    def readInt(self) -> int:
        return int.from_bytes(self.stream.read(4), "little")

    def readShort(self) -> int:
        return int.from_bytes(self.stream.read(2), "little", signed=True)

    def readUShort(self) -> int:
        return int.from_bytes(self.stream.read(2), "little", signed=False)

    def readByte(self) -> int:
        return int.from_bytes(self.stream.read(1), "little", signed=True)

    def readUByte(self) -> int:
        return int.from_bytes(self.stream.read(1), "little", signed=False)

    def readSingle(self) -> float:
        return struct.unpack('f', self.stream.read(4))[0]

    def readVector2(self) -> Vector2:
        vec = Vector2()
        vec.x = self.readSingle()
        vec.y = self.readSingle()
        return vec

    def readVector3(self) -> Vector3:
        vec = Vector3()
        vec.x = self.readSingle()
        vec.y = self.readSingle()
        vec.z = self.readSingle()
        return vec

    def readQuaternion(self) -> Quaternion:
        vec = Quaternion()
        vec.x = self.readSingle()
        vec.y = self.readSingle()
        vec.z = self.readSingle()
        vec.w = self.readSingle()
        return vec

    def readDouble(self) -> float:
        return struct.unpack('d', self.stream.read(8))[0]

    def readBytes(self, length):
        return self.stream.read(length)

    def writeInt(self, value: int):
        self.stream.write(value.to_bytes(4, "little"))

    def writeShort(self, value: int):
        self.stream.write(value.to_bytes(2, "little"))

    def writeByte(self, value: int):
        self.stream.write(value.to_bytes(1, "little"))

    def writeSingle(self, value: float):
        self.stream.write(struct.pack('f', value))

    def writeString(self, value: str, length: int):
        b = bytes(value, 'ascii')
        self.stream.write(b)
        x = length - len(value)
        while x != 0:
            self.stream.write(bytes('\0', 'ascii'))
            x -= 1

    def tell(self) -> int:
        return self.stream.tell()

    def seek(self, offset: int, __whence: int = 0) -> int:
        return self.stream.seek(offset, __whence)

    def close(self):
        self.stream.close()

    def __len__(self):
        return self.length


class BinaryFile(BinaryStream):
    def __init__(self, path: object) -> object:
        super().__init__()
        self.name = path
        self.length = os.stat(path).st_size
        try:
            self.stream = open(path, "rb")
        except IOError:
            print("Could not open file for reading:\n %s" % path)
            exit(1)

class BinaryFileWrite(BinaryStream):
    def __init__(self, path):
        super().__init__()
        self.name = path
        self.length = -1
        try:
            self.stream = open(path, "wb")
        except IOError:
            print("Could not open file for reading:\n %s" % path)
            exit(1)

class BinaryArrayStream(BinaryStream):
    def __init__(self, data: bytes):
        super().__init__()
        self.stream = io.BytesIO(data)
        self.length = len(data)

