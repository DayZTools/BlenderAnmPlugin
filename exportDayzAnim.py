import time
import bpy
from bpy.props import StringProperty, CollectionProperty, EnumProperty, BoolProperty
from bpy_extras.io_utils import ExportHelper


class ExportDayzAnim(bpy.types.Operator, ExportHelper):
    bl_idname = "export_scene.dayzanm"
    bl_label = "Export DayZ .anm"
    bl_description = "Export an DayZ animation"
    bl_options = {'PRESET'}

    filename_ext = ".anm"
    filter_glob: StringProperty(default="*.anm", options={'HIDDEN'})

    files: CollectionProperty(type=bpy.types.PropertyGroup)

    key_types: EnumProperty(
        name="Keyframe Types",
        description="Export specific keyframe types",
        options={'ENUM_FLAG'},
        items=(('LOC', "Location", ""),
               ('ROT', "Rotation", ""),
               ('SCALE', "Scale", ""),
               ),
        default={'LOC', 'ROT', 'SCALE'},
    )

    every_frame: BoolProperty(
        name="Every Frame",
        description="Automatically generate keyframes for every single frame",
        default=False)

    use_actions: BoolProperty(
        name="Export All Actions",
        description="Export all actions to the target path",
        default=False)

    # PREFIX & SUFFIX Require "use_actions" to be true and are enabled /
    # disabled from __update_use_actions
    prefix: StringProperty(
        name="File Prefix",
        description="The prefix string that is applied to the beginning of the filename for each exported action",
        default="")

    suffix: StringProperty(
        name="File Suffix",
        description="The suffix string that is applied to the end of the filename for each exported action",
        default="")

    def draw(self, context):
        layout = self.layout

        row = layout.row()
        row.label(text="Include:")
        row.prop(self, "key_types")

        layout.prop(self, "every_frame")

        box = layout.box()
        box.prop(self, "use_actions")
        if self.use_actions:
            box.prop(self, "prefix")
            box.prop(self, "suffix")

    def execute(self, context):
        from . import DayZAnim_export
        start_time = time.process_time()
        result = DayZAnim_export.save(self, context)
        if not result:
            self.report({'INFO'}, "Export finished in %.4f sec." % (time.process_time() - start_time))
            return {'FINISHED'}
        else:
            self.report({'ERROR'}, result)
            return {'CANCELLED'}

    @classmethod
    def poll(self, context):
        ob = context.active_object
        if ob is not None:
            if ob.type == 'ARMATURE' and ob.animation_data is not None:
                return True
        return False
