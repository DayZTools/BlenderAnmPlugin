from typing import Generic, TypeVar, Union, NewType

from dayzAnmPlugin.BinaryFile import BinaryStream
from dayzAnmPlugin.Entity.Entity import Entity

T = TypeVar('T', bound=Entity)
NT = NewType('NT', Entity)


class FormEntity(Entity, Generic[T]):
    def __init__(self, entity):
        super().__init__()
        self.Data: Union[Entity, None] = entity
        self.ExpectedTag = 'FORM'

    def readData(self, file: BinaryStream):
        self.Data.read(file)
        self.checkAllRead(file)

    def writeData(self, file: BinaryStream):
        self.Data.write(file)

    def checkAllRead(self, file: BinaryStream):
        if file.tell() != len(file):
            raise Exception('Not all data was read')
