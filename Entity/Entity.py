from dayzAnmPlugin.BinaryFile import BinaryStream


class Entity:
    DefaultTagLength = 4

    def __init__(self):
        self.Tag: str = ''
        self.TagLength: int = Entity.DefaultTagLength
        self.ExpectedTag: str = ''
        self.EntitySize: int = 0
        self.DataOffset: int = 0

    def read(self, file: BinaryStream):
        if file.tell() >= len(file):
            return
        self.Tag = file.readString(self.TagLength)
        self.EntitySize = self.byteSwap(file.readInt())
        self.DataOffset = file.tell()
        self.validateTag()
        print('Reading ' + self.Tag)
        self.readData(file)

    def validateTag(self):
        if self.ExpectedTag != self.Tag:
            raise Exception(f'Got invalid tag: {self.Tag} expected: {self.ExpectedTag}')

    def readData(self, file: BinaryStream):
        off = self.EntitySize - (file.tell() - self.DataOffset)
        file.seek(off, 1)

    def write(self, file: BinaryStream):
        file.writeString(self.ExpectedTag, self.TagLength)
        file.writeInt(self.byteSwap(0))  # will-be overwritten
        self.DataOffset = file.tell()
        self.writeData(file)
        self.writeSize(file)

    def writeData(self, file: BinaryStream):
        pass

    def writeSize(self, file: BinaryStream):
        off = file.tell()
        file.seek(self.DataOffset - 4, 0)
        file.writeInt(self.byteSwap(off - self.DataOffset))
        file.seek(off, 0)

    @staticmethod
    def byteSwap(val: int) -> int:
        intVal = val >> 16 | val << 16
        return (intVal & 0xFF00_FF00) >> 8 | ((intVal & 0x00FF_00FF) << 8)
