from typing import Union, List

from dayzAnmPlugin.BinaryFile import BinaryFile, BinaryStream
from dayzAnmPlugin.Data.Model import Lod, Mesh, BoneTree, UnkBoneClass
from dayzAnmPlugin.Entity.Entity import Entity
from dayzAnmPlugin.Entity.Model.LodsEntity import LodsEntity


class ModelEntity(Entity):
    def __init__(self):
        super().__init__()
        self.ExpectedTag = 'XOB6HEAD'
        self.TagLength = 8
        self.ModelHeads: List[Lod] = []
        self.LodsEntity: Union[LodsEntity, None] = None

    def validateTag(self):
        pass

    def readData(self, file: BinaryStream):
        unkArr = file.readBytes(44)

        materialsCount = file.readUShort()
        boneTreeCount = file.readUShort()
        lodsCount = file.readUShort()
        unk5 = file.readUShort()
        unk6 = file.readInt()
        stringTableLen = file.readInt()

        stringTable = list[str]()
        off = file.tell()
        while file.tell() < off + stringTableLen:
            stringTable.append(file.readNullString())

        materialsDict = dict[int, str]()
        for i in range(materialsCount):
            if self.Tag == 'XOB6HEAD':
                materialsDict[i] = stringTable[file.readUShort()]
            else:
                materialsDict[i] = stringTable[file.readInt()]

        boneTreeList = list[BoneTree]()

        for i in range(boneTreeCount):
            boneTree = BoneTree()

            boneTree.name = stringTable[file.readInt()]
            boneTree.position = file.readVector3()
            boneTree.rotation = file.readQuaternion()
            boneTree.leftIdx = file.readShort()
            boneTree.rightIdx = file.readShort()
            boneTreeList.append(boneTree)

        # restore parent information
        for i in range(boneTreeCount):
            if i == 0:
                boneTreeList[i].parentIdx = -1
            if boneTreeList[i].leftIdx != -1:
                boneTreeList[boneTreeList[i].leftIdx].parentIdx = boneTreeList[i].parentIdx
            if boneTreeList[i].rightIdx != -1:
                boneTreeList[boneTreeList[i].rightIdx].parentIdx = i

        for i in range(lodsCount):
            head = Lod()
            head.compressionType = 'ZLIB'
            if self.Tag == 'XOB8HEAD':
                head.compressionType = file.readString(4)
            meshesCount = file.readInt()
            head.uvVec = file.readVector2()

            if self.Tag == 'XOB6HEAD':
                h6unk1 = file.readUShort()
                head.bonesCnt = file.readUShort()
            else:
                head.bonesCnt = file.readUShort()
                h8unk1 = file.readUShort()

            head.offset = file.readInt()
            head.length = file.readInt()
            for meshIdx in range(meshesCount):
                mesh = Mesh()
                if self.Tag == 'XOB6HEAD':
                    mesh.rootName = stringTable[file.readInt()]
                    h6unk2 = file.readInt()  # always 0?
                else:
                    h8unk2 = file.readInt()
                    mesh.rootName = stringTable[file.readUShort()]
                    h8unk3 = file.readUShort()  # always 0?

                unkArr2 = file.readBytes(40)
                mesh.facesCount = file.readUShort()
                mesh.verticesCount = file.readUShort()
                mesh.unkCnt = file.readUShort()  # 12 * unkCnt data size (Vector3?)
                mesh.weightsCount = file.readUShort()  # 16 * unkCnt1 data size (Quaternions?)
                mesh.unkCnt2 = file.readUShort()  # unknown, always 0 (Vector2 (UV)?)
                if self.Tag == 'XOB6HEAD':
                    mesh.materialCount = file.readUShort()
                    h6unk3 = file.readUShort()  # always 65535? parent material?
                    mesh.materialName = materialsDict[file.readUShort()]
                else:
                    h8unk4 = file.readUShort()  # always 65535?
                    mesh.materialName = materialsDict[file.readUShort()]
                    mesh.materialCount = file.readUByte()

                mesh.bonesDataCnt = file.readUByte()
                hunk = file.readUByte()  # not null if bonesDataCnt either not 0 (known values 0, 4, 255)
                off = file.tell()
                xArr = file.readBytes(3)  # alignment?

                if self.Tag == 'XOB6HEAD':
                    h6unkArr3 = file.readBytes(3)  # alignment?
                else:
                    h8UnkArr3 = file.readBytes(20)

                for x in range(0, mesh.bonesDataCnt):
                    mesh.boneIndexes[x] = file.readUByte()

                head.meshes.append(mesh)

            for _ in range(head.bonesCnt):
                bunk = UnkBoneClass()
                bunk.unk1 = file.readUShort()
                bunk.unk2 = file.readUShort()
                bunk.vunk1 = file.readVector3()
                bunk.vunk2 = file.readVector3()
                head.bunks.append(bunk)
            self.ModelHeads.append(head)

        self.LodsEntity = LodsEntity(self.ModelHeads)
        self.LodsEntity.read(file)

    def writeData(self, file: BinaryFile):
        pass
