import zlib
from typing import List

from dayzAnmPlugin.BinaryFile import BinaryArrayStream, BinaryStream
from dayzAnmPlugin.Data.BasicTypes import Vector2
from dayzAnmPlugin.Data.Model import Lod, Mesh, Vertex
from dayzAnmPlugin.Entity.Entity import Entity


class Face:
    def __init__(self):
        self.x: int = 0
        self.y: int = 0
        self.z: int = 0


class BoneWeight:
    def __init__(self, idx: int, weight: float):
        self.boneIndex: int = idx
        self.boneWeight: float = weight


class LodsEntity(Entity):
    def __init__(self, modelHeads):
        super().__init__()
        self.ExpectedTag = 'LODS'
        self.ModelHeads: List[Lod] = modelHeads
        self.faces: dict[int, List[Face]] = dict[int, list[Face]]()
        self.vertices: dict[int, List[Vertex]] = dict[int, list[Vertex]]()
        self.weights: dict[int, dict[int, list[BoneWeight]]] = dict[int, dict[int, list[BoneWeight]]]()

    def readData(self, file: BinaryStream):
        for head in self.ModelHeads:
            file.seek(head.offset)
            if head.compressionType != 'ZLIB':
                raise Exception(f'Not implemented compression type: {head.compressionType}')
            data = zlib.decompress(file.readBytes(head.length))
            stream = BinaryArrayStream(data)
            decLength = len(stream)

            for meshId, mesh in enumerate(head.meshes):
                off1 = stream.tell()
                self.readFaces(stream, mesh, meshId)
                unkArr = []
                for _ in range(mesh.facesCount):
                    unkArr.append([stream.readUShort(), stream.readUShort(), stream.readUShort()])
                self.readVertices(stream, mesh, meshId)
                unkArr2 = []
                for _ in range(mesh.unkCnt):
                    unkArr2.append([stream.readInt(), stream.readInt(), stream.readInt()])
                self.readWeights(stream, mesh, meshId)
                off2 = stream.tell()
                pass

    def readFaces(self, stream: BinaryStream, mesh: Mesh, meshId: int):
        for _ in range(mesh.facesCount):
            face = Face()
            face.x = stream.readUShort()
            face.y = stream.readUShort()
            face.z = stream.readUShort()
            if meshId not in self.faces.keys():
                self.faces[meshId] = list[Face]()
            self.faces[meshId].append(face)

    def readVertices(self, stream: BinaryStream, mesh: Mesh, meshId: int):
        for _ in range(mesh.verticesCount):
            vertex = Vertex()

            vertex.position = stream.readVector3()

            if mesh.bonesDataCnt > 0:
                vertex.weightIndex = stream.readInt()

            if meshId not in self.vertices.keys():
                self.vertices[meshId] = list[Vertex]()
            self.vertices[meshId].append(vertex)

        for vertIdx in range(mesh.verticesCount):
            num = stream.readInt()

        for vertIdx in range(mesh.verticesCount):
            for matIdx in range(mesh.materialCount):
                x = stream.readShort()
                y = stream.readShort()
                uv = Vector2()
                uv.x = x / 1024.0
                uv.y = 1.0 - y / 1024.0
                self.vertices[meshId][vertIdx].uvLayers.append(uv)

    def readWeights(self, stream: BinaryStream, mesh: Mesh, meshId: int):
        if mesh.bonesDataCnt <= 0:
            return
        savedPos = stream.tell()

        for vertIdx in range(mesh.verticesCount):
            # search weight data
            stream.seek(savedPos + (self.vertices[meshId][vertIdx].weightIndex * 16), 0)
            boneId1 = mesh.boneIndexes[stream.readUByte()]
            boneId2 = mesh.boneIndexes[stream.readUByte()]
            boneId3 = mesh.boneIndexes[stream.readUByte()]
            boneId4 = mesh.boneIndexes[stream.readUByte()]
            weight1 = stream.readSingle()
            weight2 = stream.readSingle()
            weight3 = stream.readSingle()
            weight4 = 1.0 - (weight1 + weight2 + weight3)

            weights = list[BoneWeight]()
            weights.append(BoneWeight(boneId1, weight1))

            if weight2 > 0.0:
                weights.append(BoneWeight(boneId2, weight2))

            if weight3 > 0.0:
                weights.append(BoneWeight(boneId3, weight3))

            if weight4 > 0.0 and boneId4 != boneId1 and boneId4 != boneId2 and boneId4 != boneId3:
                weights.append(BoneWeight(boneId4, weight4))

            if meshId not in self.weights.keys():
                self.weights[meshId] = dict[int, list[BoneWeight]]()

            self.weights[meshId][vertIdx] = weights
            pass
            # unkArr2 = []
            # for _ in range(mesh.unkCnt1):
            #     unkArr2.append([stream.readInt(), stream.readInt(), stream.readInt(), stream.readInt()])

        off = stream.tell()
        stream.seek(savedPos + (mesh.weightsCount * 16), 0)
        off2 = stream.tell()
        pass
