from typing import List, Union

from dayzAnmPlugin.BinaryFile import BinaryStream
from dayzAnmPlugin.Data.Animation import BoneAnimation, BoneHead, Vector3, Quaternion, KeyFrame
from dayzAnmPlugin.Entity.Animation.HeadEntity import HeadEntity
from dayzAnmPlugin.Entity.Entity import Entity


class DataEntity(Entity):
    RadPerVal = 1.0 / 65535

    def __init__(self, headEntity):
        super().__init__()
        self.ExpectedTag = "DATA"
        self.headEntity: HeadEntity = headEntity
        self.animationData: dict[str, BoneAnimation] = {}

    def readData(self, file: BinaryStream):
        for head in self.headEntity.heads:
            boneAnim = BoneAnimation()
            boneAnim.frameCount = head.numFrames
            boneAnim.translations = self.readPositions(file, head)
            boneAnim.scales = self.readScales(file, head)
            boneAnim.rotations = self.readRotations(file, head)
            self.animationData[head.name] = boneAnim

    def writeData(self, file: BinaryStream):
        for head in self.headEntity.heads:
            boneAnim = self.animationData[head.name]
            self.writePositions(file, head, boneAnim)
            self.writeScales(file, head, boneAnim)
            self.writeRotations(file, head, boneAnim)

    @staticmethod
    def readPositions(file: BinaryStream, head: BoneHead) -> List[KeyFrame]:
        if head.translationFrameCount <= 0:
            return []
        translations = list[KeyFrame]()

        ids: List[int] = []
        for _ in range(head.translationFrameCount):
            ids.append(file.readUShort())

        multiplier = head.translationMultiplier * DataEntity.RadPerVal

        for index in range(head.translationFrameCount):
            translation = Vector3()
            translation.x = file.readUShort() * multiplier + head.translationBias
            translation.y = file.readUShort() * multiplier + head.translationBias
            translation.z = file.readUShort() * multiplier + head.translationBias
            translations.append(KeyFrame(ids[index], translation))
        return translations

    @staticmethod
    def readScales(file: BinaryStream, head: BoneHead) -> List[KeyFrame]:
        if head.scaleFrameCount <= 0:
            return []

        scales = list[KeyFrame]()

        ids: List[int] = []
        for _ in range(head.scaleFrameCount):
            ids.append(file.readUShort())

        multiplier = head.scaleMultiplier * DataEntity.RadPerVal
        index = 0

        for index in range(head.scaleFrameCount):
            scale = Vector3()
            scale.x = file.readUShort() * multiplier + head.scaleBias
            scale.y = file.readUShort() * multiplier + head.scaleBias
            scale.z = file.readUShort() * multiplier + head.scaleBias
            scales.append(KeyFrame(ids[index], scale))
        return scales

    @staticmethod
    def readRotations(file: BinaryStream, head: BoneHead) -> List[KeyFrame]:
        if head.rotationFrameCount <= 0:
            return []

        rotations = list[KeyFrame]()

        ids: List[int] = []
        for _ in range(head.rotationFrameCount):
            ids.append(file.readUShort())

        multiplier = head.rotationMultiplier * DataEntity.RadPerVal

        for index in range(head.rotationFrameCount):
            rotation = Quaternion()
            rotation.x = file.readUShort() * multiplier + head.rotationBias
            rotation.y = file.readUShort() * multiplier + head.rotationBias
            rotation.z = file.readUShort() * multiplier + head.rotationBias
            rotation.w = file.readUShort() * multiplier + head.rotationBias
            rotations.append(KeyFrame(ids[index], rotation))
        return rotations

    @staticmethod
    def writePositions(file: BinaryStream, head: BoneHead, anim: BoneAnimation):
        if len(anim.translations) == 0:
            return

        # write keyframes
        for animFrame in anim.translations:
            file.writeShort(animFrame.frame)

        multiplier = 1
        if head.translationMultiplier > 0.0:
            multiplier = 65535 / head.translationMultiplier

        for frame in anim.translations:
            file.writeShort(DataEntity.compress(frame.data.x, head.translationBias, multiplier))
            file.writeShort(DataEntity.compress(frame.data.y, head.translationBias, multiplier))
            file.writeShort(DataEntity.compress(frame.data.z, head.translationBias, multiplier))

    @staticmethod
    def writeScales(file: BinaryStream, head: BoneHead, anim: BoneAnimation):
        if len(anim.scales) == 0:
            return

        # write keyframes
        for animFrame in anim.scales:
            file.writeShort(animFrame.frame)

        multiplier = 1
        if head.scaleMultiplier > 0.0:
            multiplier = 65535 / head.scaleMultiplier

        for frame in anim.scales:
            file.writeShort(DataEntity.compress(frame.data.x, head.scaleBias, multiplier))
            file.writeShort(DataEntity.compress(frame.data.y, head.scaleBias, multiplier))
            file.writeShort(DataEntity.compress(frame.data.z, head.scaleBias, multiplier))

    @staticmethod
    def writeRotations(file: BinaryStream, head: BoneHead, anim: BoneAnimation):
        if len(anim.rotations) == 0:
            return

        # write keyframes
        for animFrame in anim.rotations:
            file.writeShort(animFrame.frame)

        multiplier = 1
        if head.rotationMultiplier > 0.0:
            multiplier = 65535 / head.rotationMultiplier

        for frame in anim.rotations:
            file.writeShort(DataEntity.compress(frame.data.x, head.rotationBias, multiplier))
            file.writeShort(DataEntity.compress(frame.data.y, head.rotationBias, multiplier))
            file.writeShort(DataEntity.compress(frame.data.z, head.rotationBias, multiplier))
            file.writeShort(DataEntity.compress(frame.data.w, head.rotationBias, multiplier))

    @staticmethod
    def compress(value: float, bias: float, multiplier: float) -> int:
        return int(min(max((value - bias) * multiplier, 0.0), 65535.0))
