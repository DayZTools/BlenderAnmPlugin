from dayzAnmPlugin.BinaryFile import BinaryStream
from dayzAnmPlugin.Entity.Entity import Entity


class FpsEntity(Entity):

    def __init__(self):
        super().__init__()
        self.ExpectedTag = "FPS"
        self.fps: int = 0

    def readData(self, file: BinaryStream):
        self.fps = file.readInt()

    def writeData(self, file: BinaryStream):
        file.writeInt(self.fps)
