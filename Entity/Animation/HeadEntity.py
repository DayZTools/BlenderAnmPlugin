from typing import List

from dayzAnmPlugin.BinaryFile import BinaryStream
from dayzAnmPlugin.Data.Animation import BoneHead
from dayzAnmPlugin.Entity.Entity import Entity


class HeadEntity(Entity):
    def __init__(self, entryType: str):
        super().__init__()
        self.heads: List[BoneHead] = []
        self.EntryType = entryType
        self.ExpectedTag = 'HEAD'

    def readData(self, file: BinaryStream):
        if self.EntryType == 'ANIMSET5':
            self.read5(file)
        elif self.EntryType == 'ANIMSET6':
            self.read6(file)
        else:
            raise Exception(f'Unknown model type: {self.EntryType}')

    def writeData(self, file: BinaryStream):
        if self.EntryType == 'ANIMSET5':
            self.write5(file)
        elif self.EntryType == 'ANIMSET6':
            self.write6(file)
        else:
            raise Exception(f'Unknown model type: {self.EntryType}')

    def read5(self, file: BinaryStream):
        while file.tell() < self.EntitySize + self.DataOffset:
            head = BoneHead()
            head.name = file.readString(32)
            head.translationBias = file.readSingle()
            head.translationMultiplier = file.readSingle()
            head.rotationBias = file.readSingle()
            head.rotationMultiplier = file.readSingle()
            head.numFrames = file.readUShort()
            head.translationFrameCount = file.readUShort()
            head.rotationFrameCount = file.readUShort()
            head.flags = file.readUShort()
            head.scaleFrameCount = 0
            head.scaleMultiplier = 0.0
            head.scaleBias = 0.0
            self.heads.append(head)

    def read6(self, file: BinaryStream):
        while file.tell() < self.EntitySize + self.DataOffset:
            head = BoneHead()
            head.translationBias = file.readSingle()
            head.translationMultiplier = file.readSingle()
            head.rotationBias = file.readSingle()
            head.rotationMultiplier = file.readSingle()
            head.scaleBias = file.readSingle()
            head.scaleMultiplier = file.readSingle()
            head.numFrames = file.readUShort()
            head.translationFrameCount = file.readUShort()
            head.rotationFrameCount = file.readUShort()
            head.scaleFrameCount = file.readUShort()
            head.flags = file.readUByte()
            head.name = file.readString(file.readUByte())
            self.heads.append(head)

    def write5(self, file: BinaryStream):
        for bone in self.heads:
            file.writeString(bone.name, 32)
            file.writeSingle(bone.translationBias)
            file.writeSingle(bone.translationMultiplier)
            file.writeSingle(bone.rotationBias)
            file.writeSingle(bone.rotationMultiplier)
            file.writeShort(bone.numFrames)
            file.writeShort(bone.translationFrameCount)
            file.writeShort(bone.rotationFrameCount)
            file.writeShort(bone.flags)

    def write6(self, file: BinaryStream):
        for bone in self.heads:
            file.writeSingle(bone.translationBias)
            file.writeSingle(bone.translationMultiplier)
            file.writeSingle(bone.rotationBias)
            file.writeSingle(bone.rotationMultiplier)
            file.writeSingle(bone.scaleBias)
            file.writeSingle(bone.scaleMultiplier)
            file.writeShort(bone.numFrames)
            file.writeShort(bone.translationFrameCount)
            file.writeShort(bone.rotationFrameCount)
            file.writeShort(bone.scaleFrameCount)
            file.writeByte(bone.flags)
            file.writeByte(len(bone.name))
            file.writeString(bone.name, len(bone.name))
