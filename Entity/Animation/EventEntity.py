from typing import List

from dayzAnmPlugin.BinaryFile import BinaryStream
from dayzAnmPlugin.Data.Animation import AnimEvent
from dayzAnmPlugin.Entity.Entity import Entity


class EventEntity(Entity):
    def __init__(self):
        super().__init__()
        self.ExpectedTag = "EVNT"
        self.events: List[AnimEvent] = []

    def readData(self, file: BinaryStream):
        count = file.readUShort()
        index = 0
        while index < count:
            event = AnimEvent()
            event.frame = file.readInt()
            event.name = file.readString(file.readInt())
            event.str2 = file.readString(file.readInt())
            event.unk1 = file.readInt()
            self.events.append(event)
            index += 1
        pass

    def writeData(self, file: BinaryStream):
        file.writeShort(len(self.events))
        for event in self.events:
            file.writeInt(event.frame)
            file.writeInt(len(event.name) + 1)
            file.writeString(event.name, len(event.name) + 1)
            file.writeInt(len(event.str2) + 1)
            file.writeString(event.str2, len(event.str2) + 1)
            file.writeInt(event.unk1)