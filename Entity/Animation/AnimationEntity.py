from typing import Union, List

from dayzAnmPlugin.BinaryFile import BinaryStream
from dayzAnmPlugin.Data.Animation import BoneAnimation, AnimEvent, BoneHead, KeyFrame
from dayzAnmPlugin.Data.BasicTypes import Vector3, Quaternion
from dayzAnmPlugin.Entity.Animation.DataEntity import DataEntity
from dayzAnmPlugin.Entity.Animation.EventEntity import EventEntity
from dayzAnmPlugin.Entity.Animation.FpsEntity import FpsEntity
from dayzAnmPlugin.Entity.Animation.HeadEntity import HeadEntity
from dayzAnmPlugin.Entity.Entity import Entity


class AnimationEntity(Entity):

    def __init__(self):
        super().__init__()
        self.FpsEntity: Union[FpsEntity, None] = None
        self.HeadEntity: Union[HeadEntity, None] = None
        self.DataEntity: Union[DataEntity, None] = None
        self.EventEntity: Union[EventEntity, None] = None
        self.TagLength = 8
        self.ExpectedTag = "ANIMSET6"

    def validateTag(self):
        pass  # to override error

    def readData(self, file: BinaryStream):
        self.FpsEntity = FpsEntity()
        self.FpsEntity.read(file)

        self.HeadEntity = HeadEntity(self.Tag)
        self.HeadEntity.read(file)

        self.DataEntity = DataEntity(self.HeadEntity)
        self.DataEntity.read(file)

        self.EventEntity = EventEntity()
        self.EventEntity.read(file)

    def writeData(self, file: BinaryStream):
        self.FpsEntity.write(file)
        self.HeadEntity.write(file)
        self.DataEntity.write(file)
        if self.EventEntity is not None:
            self.EventEntity.write(file)

    def create(self, data: dict[str, BoneAnimation], events: List[AnimEvent], fps: int):
        self.FpsEntity = FpsEntity()
        self.FpsEntity.fps = fps

        self.HeadEntity = HeadEntity(self.ExpectedTag)

        for name, anim in data.items():
            bone = BoneHead()
            bone.name = name
            bone.translationFrameCount = len(anim.translations)
            bone.rotationFrameCount = len(anim.rotations)
            bone.scaleFrameCount = len(anim.scales)
            bone.numFrames = anim.frameCount
            bone.flags = 0  # unk

            if len(anim.translations) > 0:
                (minVal, maxVal) = self.findMinMaxVec(anim.translations)
                bone.translationBias = minVal
                bone.translationMultiplier = self.multi(minVal, maxVal)

            if len(anim.rotations) > 0:
                (minVal, maxVal) = self.findMinMaxQuat(anim.rotations)
                bone.rotationBias = minVal
                bone.rotationMultiplier = self.multi(minVal, maxVal)

            if len(anim.scales) > 0:
                (minVal, maxVal) = self.findMinMaxVec(anim.scales)
                bone.scaleBias = minVal
                bone.scaleMultiplier = self.multi(minVal, maxVal)

            self.HeadEntity.heads.append(bone)

            self.DataEntity = DataEntity(self.HeadEntity)
            self.DataEntity.animationData = data

            if len(events) > 0:
                self.EventEntity = EventEntity()
                self.EventEntity.events = events

    def multi(self, minVal: float, maxVal: float):
        epsilon: float = 9.9999999747524271E-07
        if abs(maxVal - minVal) < epsilon:
            return 1.0
        return maxVal - minVal

    def findMinMaxVec(self, vectors: list[KeyFrame]) -> (float, float):
        minVal = 0
        maxVal = 0
        for v in vectors:
            minVal = min(minVal, v.data.x)
            minVal = min(minVal, v.data.y)
            minVal = min(minVal, v.data.z)

            maxVal = max(maxVal, v.data.x)
            maxVal = max(maxVal, v.data.y)
            maxVal = max(maxVal, v.data.z)
        return minVal, maxVal

    def findMinMaxQuat(self, vectors: list[KeyFrame]) -> (float, float):
        minVal = 0
        maxVal = 0
        for v in vectors:
            minVal = min(minVal, v.data.x)
            minVal = min(minVal, v.data.y)
            minVal = min(minVal, v.data.z)
            minVal = min(minVal, v.data.w)

            maxVal = max(maxVal, v.data.x)
            maxVal = max(maxVal, v.data.y)
            maxVal = max(maxVal, v.data.z)
            maxVal = max(maxVal, v.data.w)
        return minVal, maxVal
