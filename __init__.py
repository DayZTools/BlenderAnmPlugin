import bpy
from bpy.props import *

from dayzAnmPlugin.exportDayzAnim import ExportDayzAnim
from dayzAnmPlugin.importDayzAnim import ImportDayzAnim

bl_info = {
    "name": "DayZ ANM Support",
    "author": "Stanislav \"Koncord\" Zhukov",
    "version": (0, 4, 0),
    "blender": (3, 1, 0),
    "location": "File > Import",
    "description": "Import DayZ Animations & XOB6/8 models",
    "support": "COMMUNITY",
    "category": "Import-Export"
}

def menu_func_dayz_anm_import(self, context):
    self.layout.operator(ImportDayzAnim.bl_idname, text="DayZ ANM (.anm)")


def menu_func_dayz_anm_export(self, context):
    self.layout.operator(ExportDayzAnim.bl_idname, text="DayZ ANM (.anm)")


io_collection = (
    ImportDayzAnim,
    ExportDayzAnim
)

def register():
    for cls in io_collection:
        bpy.utils.register_class(cls)
    bpy.types.TOPBAR_MT_file_import.append(menu_func_dayz_anm_import)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_dayz_anm_export)


def unregister():
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_dayz_anm_import)
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_dayz_anm_export)
    for cls in io_collection:
        bpy.utils.unregister_class(cls)


if __name__ == "__main__":
    register()

