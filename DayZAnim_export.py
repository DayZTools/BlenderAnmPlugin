import os

import bpy
import bpy_types
from bpy_extras.wm_utils.progress_report import ProgressReport
from dayzAnmPlugin.BinaryFile import BinaryFile, BinaryFileWrite
from dayzAnmPlugin.Data.Animation import BoneAnimation, KeyFrame, AnimEvent
from dayzAnmPlugin.Data.BasicTypes import Vector3
from dayzAnmPlugin.Entity.Animation.AnimationEntity import AnimationEntity
from dayzAnmPlugin.Entity.FormEntity import FormEntity
from mathutils import *


def save(self, context, filepath=""):
    ob = bpy.context.object
    if ob.type != 'ARMATURE':
        return "An armature must be selected!"
    prefix = self.prefix
    suffix = self.suffix

    path = os.path.dirname(self.filepath)
    path = os.path.normpath(path)

    # Gets automatically updated per-action if self.use_actions is true, otherwise it stays the same
    filepath = self.filepath

    with ProgressReport(context.window_manager) as progress:
        actions = []
        if self.use_actions:
            actions = bpy.data.actions
        else:
            actions = [bpy.context.object.animation_data.action]

        progress.enter_substeps(len(actions))

        for action in actions:
            if self.use_actions:
                filename = prefix + action.name + suffix + ".anm"
                filepath = os.path.normpath(os.path.join(path, filename))

            progress.enter_substeps(1, action.name)
            try:
                export_action(self, context, progress, action, filepath)
            except Exception as e:
                progress.leave_substeps("ERROR: " + repr(e))
            else:
                progress.leave_substeps()

        progress.leave_substeps("Finished!")


def get_rot_quat(bone):
    # Absolute, Relative, and Delta all use the same rotation formula
    try:
        bone.parent.matrix
    except:
        # Let's just return the matrix as a quaternion for now - it mirrors what
        # the importer does
        return bone.matrix.to_quaternion()
    else:
        mtx = bone.parent.matrix.to_3x3()
        return (mtx.inverted() @ bone.matrix.to_3x3()).to_quaternion()

def export_action(self, context, progress, action, filepath):
    ob = bpy.context.object
    frame_original = context.scene.frame_current

    frameStart = int(action.frame_range[0])
    frameCount = int(action.frame_range[1]) - int(action.frame_range[0]) + 1
    use_keys_loc = 'LOC' in self.key_types
    use_keys_rot = 'ROT' in self.key_types
    use_keys_scale = 'SCALE' in self.key_types

    anim_bones: dict[str, BoneAnimation] = {}

    for pose_bone in ob.pose.bones:
        bone = BoneAnimation()
        bone.frameCount = frameCount
        anim_bones[pose_bone.name] = bone

    frames = {}

    # Step 1: Analyzing Keyframes
    # Resolve the relevant keyframe indices for loc, rot, and / or scale for each bone
    for fc in action.fcurves:
        try:
            prop = ob.path_resolve(fc.data_path, False)  # coerce=False
            if type(prop.data) != bpy_types.PoseBone:
                raise
            pose_bone = prop.data

            if prop == pose_bone.location.owner:
                if not use_keys_loc:
                    continue
                # print("LOC")
                index = 0
            elif (prop == pose_bone.rotation_quaternion.owner or
                  prop == pose_bone.rotation_euler.owner or
                  prop == pose_bone.rotation_axis_angle.owner):
                if not use_keys_rot:
                    continue
                # print("ROT")
                index = 1
            elif owner is pose_bone.scale.owner:
                if not use_keys_scale:
                    continue
                # print("SCALE")
                index = 2
            else:  # If the fcurve isn't for a valid property, just skip it
                continue
        except Exception as e:
            # The fcurve probably isn't even for a pose bone - just skip it
            # print("skipping : %s" % e) # DEBUG
            pass
        else:
            for key in fc.keyframe_points:
                f = int(key.co[0])
                if frames.get(f) is None:
                    frames[f] = {}
                frame_bones = frames[f]
                if frame_bones.get(pose_bone.name) is None:
                    # [PoseBone, LocKey, RotKey, ScaleKey] for each bone on the current frame
                    frame_bones[pose_bone.name] = [
                        pose_bone, False, False, False]
                # Enable the corresponding keyframe type for that bone on this frame
                frame_bones[pose_bone.name][index + 1] = True
    # Set the frame_count to the REAL number of frames in the action if there is only 1
    if len(frames) == 1:
        for name, bone in anim_bones.items():
            bone.frameCount = 1
            frameCount = 1

    # Step 2: Gathering Animation Data
    if self.every_frame:  # Export every keyframe
        progress.enter_substeps(frameCount)
        for frame in range(frameCount):
            context.scene.frame_set(frame + frameStart)
            for name, bone in anim_bones.items():
                pose_bone = ob.pose.bones.get(name)
                if pose_bone is None:
                    continue

                if use_keys_loc:
                    data = Vector3()
                    loc = pose_bone.matrix_basis.translation
                    data.x = loc.x
                    data.y = loc.y
                    data.z = loc.z
                    bone.translations.append(KeyFrame(frame, data))
                if use_keys_scale:
                    data = Vector3()
                    scale = pose_bone.matrix_basis.translation
                    data.x = scale.x
                    data.y = scale.y
                    data.z = scale.z
                    bone.scales.append(KeyFrame(frame, data))
                if use_keys_rot:
                    data = Quaternion()
                    quat = get_rot_quat(pose_bone)
                    data.x = quat.x
                    data.y = quat.y
                    data.z = quat.z
                    data.w = quat.w
                    bone.rotations.append(KeyFrame(frame, data))
            progress.step()
    else:  # Only export keyed frames
        progress.enter_substeps(len(frames))

        for frame, bones in frames.items():
            context.scene.frame_set(frame)

            for name, bone_info in bones.items():
                # the first element in the bone_info array is the PoseBone
                pose_bone = bone_info[0]
                bone = anim_bones[name]

                if use_keys_loc and bone_info[1]:
                    data = Vector3()
                    loc = pose_bone.matrix_basis.translation
                    data.x = loc.x
                    data.y = loc.y
                    data.z = loc.z
                    bone.translations.append(KeyFrame(frame - frameStart, data))
                if use_keys_scale:
                    data = Vector3()
                    scale = pose_bone.scale
                    data.x = scale.x
                    data.y = scale.y
                    data.z = scale.z
                    bone.scales.append(KeyFrame(frame - frameStart, data))
                if use_keys_rot:
                    data = Quaternion()
                    quat = get_rot_quat(pose_bone)
                    data.x = quat.x
                    data.y = quat.y
                    data.z = quat.z
                    data.w = quat.w
                    bone.rotations.append(KeyFrame(frame - frameStart, data))
            progress.step()

    context.scene.frame_set(frame_original)
    progress.leave_substeps()

    # Step 3: Finalizing Data
    events: list[AnimEvent] = []

    for pose_marker in action.pose_markers:
        note = AnimEvent()
        note.frame = pose_marker.frame
        x = pose_marker.name.split('|')
        note.name = x[0]
        note.str2 = x[1]
        note.unk1 = int(x[2])
        events.append(note)

    animEntity = AnimationEntity()
    animEntity.create(anim_bones, events, context.scene.render.fps)

    formEntity = FormEntity(animEntity)
    file = BinaryFileWrite(filepath)
    formEntity.write(file)
    file.close()
